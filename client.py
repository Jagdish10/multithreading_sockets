import socket
import datetime

SERVER_ADDR  = "192.168.1.67"
PORT = 8000
FORMAT = "UTF-8"

client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client.connect((SERVER_ADDR,PORT))
request_msg = "hello from client side"
client.sendall(request_msg.encode(FORMAT))
while True:
    in_data = client.recv(1024)
    print("From server {} at time {}".format(in_data.decode(FORMAT),datetime.datetime.now))
    out_data = input("enter a msg=")
    client.sendall(out_data.encode(FORMAT))
    if out_data == "DISCONNECT":
        break
client.close()