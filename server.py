import socket,threading
import datetime

'''
Server will be listening on port 8000 ,any connection established on port 8000,will be
created with a separate thread i.e each client can have a seprate thread to comunicate
with ther server.
'''

FORMAT = "UTF-8"
HOST  = socket.gethostbyname(socket.gethostname())
PORT = 8000

class ClientThread(threading.Thread):
    def __init__(self,clientAddress,clientSocket):
        threading.Thread.__init__(self,daemon=True)
        self.csocket = clientSocket
        self.caddress = clientAddress
        print("New connection added from ip {} ".format(clientAddress))
    
    def run(self):
        print("connection from {} started".format(self.caddress))
        msg = ''
        while True:
            data = self.csocket.recv(2048)
            msg = data.decode(FORMAT)
            if msg == 'DISCONNECT':
                break
            print("Message from client:{} at time {}".format(msg,datetime.datetime.now))
            response_msg = "ACKNOWLEDGED MESSAGE"
            self.csocket.send(response_msg.encode(FORMAT))
        print("Client at address {} is disconnected..".format(self.caddress))


server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
server.bind((HOST,PORT))
print("Server started at address {} ,listening at port {}".format(HOST,PORT))
print("Waiting for client connection...")
while True:
    server.listen(1)
    clientSocket,clientAddress = server.accept()
    newThread = ClientThread(clientAddress,clientSocket)
    newThread.start()


        